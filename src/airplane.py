class Airplane:
    def __init__(self, name, number_of_seats):
        self.name = name
        self.total_distance = 0
        self.number_of_seats = number_of_seats
        self.number_of_occupied_seats = 0

    def fly(self, distance):
        self.total_distance += distance

    def is_service_required(self):
        if self.total_distance > 10000:
            return True
        else:
            return False

    def board_passengers(self, number_of_passengers):
        if number_of_passengers + self.number_of_occupied_seats <= self.number_of_seats:
            self.number_of_occupied_seats += number_of_passengers
        else:
            self.number_of_occupied_seats = self.number_of_seats

    def get_available_seats(self):
        if self.number_of_occupied_seats < self.number_of_seats:
            return self.number_of_seats - self.number_of_occupied_seats
        else:
            return 0


if __name__ == '__main__':
    airplane1 = Airplane("Boeing737", 700)
    airplane2 = Airplane("Airbus", 500)
    airplane1.fly(6200)
    airplane1.fly(3799)
    airplane2.fly(6200)
    airplane2.fly(3801)
    airplane1.board_passengers(699)
    print(f'Available seats 1:{airplane1.get_available_seats()}')
    airplane2.board_passengers(499)
    print(f'Available seats 2:{airplane2.get_available_seats()}')
    print(f'Occupied seats 1:{airplane1.number_of_occupied_seats}')
    print(f'Occupied seats 2:{airplane2.number_of_occupied_seats}')
    # airplane1.board_passengers(55)
    # print(f'Available seats:{airplane1.get_available_seats()}')
    airplane2.board_passengers(501)
    airplane2.board_passengers(65)
    airplane1.get_available_seats()
    airplane2.get_available_seats()
    print(airplane1.total_distance)
    print(airplane2.total_distance)
    print(f'Is service required: {airplane1.total_distance, airplane1.is_service_required()}')
    print(f'Is service required: {airplane2.total_distance, airplane2.is_service_required()}')
    # print(f'Available seats:{airplane1.get_available_seats()}')
    # print(f'Available seats:{airplane2.get_available_seats()}')
