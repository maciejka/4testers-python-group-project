from src.airplane import Airplane


def test_if_number_of_occupied_seats_and_total_distance_equals_zero():
    airplane = Airplane("Boeing", 500)
    assert airplane.number_of_occupied_seats == 0
    assert airplane.total_distance == 0


def test_total_distance_after_5000_km():
    airplane = Airplane("Boeing", 500)
    airplane.fly(5000)
    assert airplane.total_distance == 5000


def test_total_distance_after_5000_and_3000_km():
    airplane = Airplane("Boeing", 500)
    airplane.fly(5000)
    airplane.fly(3000)
    assert airplane.total_distance == 8000


def test_is_service_required_after_9999_km():
    airplane = Airplane("Boeing", 500)
    airplane.fly(9999)
    assert not airplane.is_service_required()


def test_is_service_required_after_10001_km():
    airplane = Airplane("Boeing", 500)
    airplane.fly(10001)
    assert airplane.is_service_required()


def test_available_200_seats_after_boarded_180_passengers():
    airplane = Airplane("Boeing", 200)
    airplane.board_passengers(180)
    assert airplane.number_of_occupied_seats == 180
    assert airplane.get_available_seats() == 20


def test_available_seats_after_boarded_201_passengers():
    airplane = Airplane("Boeing", 200)
    airplane.board_passengers(201)
    assert airplane.get_available_seats() == 0
    assert airplane.number_of_occupied_seats == 200
